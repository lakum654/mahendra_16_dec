<!DOCTYPE html>
<html>
<head>
    <title>Laravel DataTables</title>
    <meta name="csrf-token" content="{{ csrf_token() }}">
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.5.0/css/bootstrap.min.css"/>
    <link href="https://cdn.datatables.net/1.10.21/css/jquery.dataTables.min.css" rel="stylesheet">
    <link href="https://cdn.datatables.net/1.10.21/css/dataTables.bootstrap4.min.css" rel="stylesheet">
</head>
<body>
<h2 class="text-center">Laravel 7|8 Yajra Datatables Example</h2>
<a href="{{ url('mahendra/create') }}" class="nav-link bg-light text-right">{{ Request::is('mahendra/create') ? 'Home/Mahendra/Add New' : '' }}</a>    
<div class="container">
    <form method="post" action="{{ route('mahendra.update',[encrypt($user->id)]) }}" enctype="multipart-form/data">
        @method('PUT')
    	@csrf()
    <div class="form-group">
    	<label>Username</label>
    	<input type="text" name="username" class="form-control" placeholder="Username" id="username" value="{{ $user->name}}">
    </div>
    <div class="form-group">
    	<label>E-Mail</label>
    	<input type="text" name="email" class="form-control" placeholder="E-Mail" id="email" value="{{ $user->email }}">
    </div>
    <div class="form-group">
    	<label>Select Roles</label>
    	<div class="row">
            @php $arrayRoles = $user->roles->pluck('id') @endphp
    		@foreach($roles as $role)
    		<div class="col-sm-3">
    			<input type="checkbox" {{ in_array($role->id,$arrayRoles->toArray()) ? 'checked' : '' }} name="role[]" value="{{ $role->id }}" class="custome-control"> {{ $role->name }}
    		</div>
    		@endforeach
    	</div>
    </div>
    <div class="form-group">
    	<input type="reset" class="btn btn-danger" value="Cancel">
    	<input type="submit" class="btn btn-success" value="Create">
    </div>	
    </form>
</div>
   
</body>

<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.9.1/jquery.js"></script>  
<script src="https://cdnjs.cloudflare.com/ajax/libs/jquery-validate/1.19.0/jquery.validate.js"></script>
<script src="https://cdn.datatables.net/1.10.21/js/jquery.dataTables.min.js"></script>
<script src="https://stackpath.bootstrapcdn.com/bootstrap/4.5.0/js/bootstrap.min.js"></script>
<script src="https://cdn.datatables.net/1.10.21/js/dataTables.bootstrap4.min.js"></script>

<script type="text/javascript">
 

</script>
</html>