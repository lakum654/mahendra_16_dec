<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Role;
use App\User;
use DataTables;
class Mahendra extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
       
        return view('mahendra.index');
    }
    public function getUsers(Request $request){
        if ($request->ajax()) {
            // $data = Post::with('user')->get();
            $data = User::with('roles')->get();
            return Datatables::of($data)
                ->addIndexColumn()
                ->addColumn('action', function($row){
                    $editUrl = '/mahendra/'.encrypt($row->id).'/edit';
                    $actionBtn = "<a href=".$editUrl." class='edit btn btn-success btn-sm'>Edit</a>";
                    return $actionBtn;
                })
                ->addColumn('role',function($row){
                    $r = [];
                    foreach($row->roles as $role){
                        array_push($r,$role->name);
                    }
                    return $r;
                })
                ->rawColumns(['action','role'])
                ->make(true);
        }
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
         $roles = Role::all();
        return view('mahendra.form',compact('roles'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
       $u = User::create(['name'=>$request->username,'email'=>$request->email]);        
       $user = User::find($u->id);   
       $roleIds = $request->role;
       $user->roles()->attach($roleIds);
       return redirect('mahendra');
        
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $id = decrypt($id);
        $user = User::find($id);
        // $r = [];
        // foreach($user->roles as $role){
        //     array_push($r,$role->name);
        // }
        // $user = [$user];
        // return $user;
        $roles = Role::all();
        return view('mahendra._form',compact('user','roles'));

    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $id = decrypt($id);
        $data = ['name'=>$request->username,'email'=>$request->email];
        $u = User::find($id)->update($data);
        $user = User::find($id);
        $roleIds = $request->role;
        //$user->roles()->detach($roleIds);
        $user->roles()->sync($roleIds);
        return redirect('mahendra');

    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
