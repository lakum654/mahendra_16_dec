<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});

Route::get('posts/','PostController@index');
Route::get('posts/create','PostController@create');
//Route::get('posts/{id}','PostController@destroy');

//return yajra index view
Route::get('posts/yajra','PostController@index2');
//return all posts with users
Route::get('getposts','PostController@getPosts')->name('posts.list');
Route::get('posts/delete','PostController@delete')->name('posts.delete');

Route::resource('mahendra','Mahendra');
Route::get('users','Mahendra@getUsers')->name('users.list');

Route::get('hasone','OneController@index');