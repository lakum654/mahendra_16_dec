<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Supplier extends Model
{
    public function orders()
    {
        return $this->hasOneThrough(
            Order::class,
            Product::class,
            'supplier_id', // Foreign key on products table...
            'product_id', // Foreign key on orders table...
            'id', // Local key on suppliers table...
            'id' // Local key on products table...
        );
    }
}
